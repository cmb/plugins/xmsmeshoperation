cmake_minimum_required(VERSION 3.20)
project(xms-mesh-operation)

include(ExternalProject)

if (NOT DEFINED SMTK_CONTRACT_TAG)
  set(SMTK_CONTRACT_TAG "origin/master")
endif()

# If on Windows, force response file usage. The command line gets way too long
# on Windows without this. Once VTK_USE_FILE and PARAVIEW_USE_FILE are gone,
# this can be removed.
set(response_file)
if (WIN32)
  set(response_file -DCMAKE_NINJA_FORCE_RESPONSE_FILE:BOOL=ON)
endif ()

ExternalProject_Add(xms-mesh-operation
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/xmsmeshoperation.git"
  GIT_TAG "${SMTK_CONTRACT_TAG}"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_PYTHON_WRAPPING=ON
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_COMMAND "${CMAKE_CTEST_COMMAND}" --output-on-failure
  TEST_BEFORE_INSTALL True
)
