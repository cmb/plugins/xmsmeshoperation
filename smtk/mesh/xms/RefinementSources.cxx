//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/mesh/xms/RefinementSources.h"


#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/GroupItemDefinition.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ModelEntityItem.h"
#include "smtk/attribute/Resource.h"

#include "smtk/model/Model.h"
#include "smtk/model/Face.h"
#include "smtk/model/Edge.h"
#include "smtk/model/EntityIterator.h"
#include "smtk/model/Tessellation.h"
#include "smtk/model/Vertex.h"

namespace smtk
{
namespace mesh
{
namespace xms
{

RefinementSources::RefinementSources()
  : m_linesPerFace()
  , m_hardEdges()
  , m_points()
{
}

void RefinementSources::addRefinementLine(const smtk::model::Face& face, const RefinementLine& line)
{
  this->m_linesPerFace[face].push_back(line);
}

void RefinementSources::addRefinementPoint(const RefinementPoint& point)
{
  this->m_points.push_back(point);
}

void RefinementSources::addHardEdge(const smtk::model::Edge& edge)
{
  this->m_hardEdges.insert(edge);
}

//----------------------------------------------------------------------------
//Get all refinement points that need to be added. This should be
//called for each face.
const std::vector< RefinementLine >& RefinementSources::lines(const smtk::model::Face& face) const
{
  //function level static data that we use to represent faces that have no
  //lines. This is used over making the m_linesPerFace mutable
  static std::vector< RefinementLine > empty_lines;

  auto i = this->m_linesPerFace.find(face);
  if ( i == this->m_linesPerFace.end())
  {
    empty_lines.resize(0); //make sure nothing ever is inside this
    return empty_lines;
  }
  return i->second;
}


//----------------------------------------------------------------------------
//Get all refinement points that need to be added. This should be
//called for each face.
//Note we will always return all the RefinementPoints, not the just the
//ones inside the given face, since outside but near a face should
//influence the refinement of that face
//Note all the points returned by this call will have valid sizing
//values ( larger than zero )
const std::vector< RefinementPoint >& RefinementSources::points(const smtk::model::Face&) const
{
  return this->m_points;
}

//----------------------------------------------------------------------------
//Determine whether or not a model edge has been declared "hard"
//by the user.
bool RefinementSources::isHardEdge(const smtk::model::Edge& edge) const
{
  return this->m_hardEdges.find(edge) != this->m_hardEdges.end();
}

}
}
}
