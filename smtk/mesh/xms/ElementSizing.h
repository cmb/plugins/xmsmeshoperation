//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_mesh_xms_ElementSizing_h
#define __smtk_mesh_xms_ElementSizing_h

#include "smtk/PublicPointerDefs.h"
#include <unordered_map>

#include "smtk/model/EntityRef.h"

namespace smtk
{
namespace model
{
class Model;
}

namespace mesh
{
namespace xms
{

// Given a model and an attribute collection look up any local size control
// for face and edge elements, and bias controls for faces.
//
// For faces we will report the face sizing, or fallback to global sizing if
// none exist. We will also report the bias controls.
//
// For edges we will report the edge sizing, or fallback to global sizing if
// none exist
class ElementSizing
{
public:
  ElementSizing();

  void setSizing(const smtk::model::EntityRef& e, double sizing);

  //Returns -1 if we have no sizing information for this entity,
  //and no global sizing.
  double sizing(const smtk::model::EntityRef& e) const;

  void setBias(const smtk::model::EntityRef& e, double bias);

  //get the bias factor for how quickly to move between
  //high and low refinement areas.
  //Note: Value will be >= 0 and <= 1.
  //Note: bias of 1.0 will be returned for invalid elements
  double bias(const smtk::model::EntityRef& e) const;

  void setGlobalSizing(double sizing) { m_globalSizing = sizing; }
  double globalSizing() const { return m_globalSizing; }


  //Note: this should exist in some other class, but that is alot of
  //effor for a single 'meta' piece of info.
  //Anyway this tells the mesher that the input loops are in CW order
  //not CCW order
  void reverse_orientation(bool choice) { m_reverseLoops = choice; }
  bool reverse_orientation() const { return m_reverseLoops; }
  void flip_orientation() { m_reverseLoops = !m_reverseLoops; }

private:
  struct EntityRefHasher
  {
    size_t operator()(const smtk::model::EntityRef& ref) const
      {
        return ref.entity().hash();
      }
  };

  typedef std::unordered_map< smtk::model::EntityRef,
                              double,
                              EntityRefHasher > MapType;


  double m_globalSizing;
  MapType m_sizings;
  MapType m_bias;

  bool m_reverseLoops; //really not sizing related
};

}
}
}

#endif
