//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include <pybind11/pybind11.h>
#include <utility>

namespace py = pybind11;

template <typename T, typename... Args>
using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

#include "PybindGenerateMesh.h"
#include "PybindRegistrar.h"

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindXMSMesh, m)
{
  m.doc() = "<description>";
  py::module smtk = m.def_submodule("smtk", "<description>");
  py::module xms = smtk.def_submodule("xms", "<description>");

  PySharedPtrClass< smtk::mesh::xms::GenerateMesh, smtk::operation::XMLOperation > smtk_xms_GenerateMesh = pybind11_init_smtk_xms_GenerateMesh(xms);

  py::class_< smtk::mesh::xms::Registrar > smtk_xms_Registrar = pybind11_init_smtk_xms_Registrar(xms);
}
