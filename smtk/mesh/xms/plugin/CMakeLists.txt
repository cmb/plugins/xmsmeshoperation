
smtk_add_plugin(
  smtkXMSMeshPlugin
  REGISTRAR smtk::mesh::xms::Registrar
  REGISTRAR_HEADER smtk/mesh/xms/Registrar.h
  MANAGERS smtk::operation::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION 1.0
)

target_link_libraries(smtkXMSMeshPlugin
  PUBLIC
  smtkCore
  smtkXMSMesh
  )
